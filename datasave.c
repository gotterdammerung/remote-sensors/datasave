#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char*argv0;
#include "arg.h"
#include "serial.h"
#include "util.h"

#include "config.h"

/* Functions declarations */
static int checkvalue(void);
static void listenport(void);
static void savedata(void);
static void usage(void);

/* Globals */
static char *serialdevice = NULL;
static int serialbaud = 0;
static char mess[13];

/* Functions implementations */
int
checkvalue()
{
	/* Check */
	if (
		(mess[0]!=',')||
		(mess[1]!='0')||
		(mess[2]!='0')||
		(mess[5]!=',')||
		(mess[6]!='0')||
		(mess[9]!=',')||
		(mess[10]!='F')
		)
		return -1;

	mess[11] = '\0';;

	return 0;
}

void
listenport()
{
	char c;

	int i;

	while (1)
	{
		/* Clear */
		c = '\0';
		memset(&mess[0], 0, sizeof(mess));

		while (serial_readchar(&c, sizeof(c)));

		switch (c)
		{
		case 'D':
			for (i=0; i<11; i++)
			{
				while (serial_readchar(&c, sizeof(c)));
				mess[i] = c;
			}
			if (!checkvalue())
				savedata();
			break;
		default:
			break;
		}
		#ifdef DEBUG
		printf("%c", c);
		#endif
	}
}

void
savedata()
{
	int temp, hume;

	char conv[3];

	FILE *file;

	/* Get data: temp */
	memset(&conv[0], 0, sizeof(conv));
	conv[0] = mess[3];
	conv[1] = mess[4];
	conv[2] = '\0';
	temp = atoi(conv);

	/* Get data: hume */
	memset(&conv[0], 0, sizeof(conv));
	conv[0] = mess[7];
	conv[1] = mess[8];
	conv[2] = '\0';
	hume = atoi(conv);


	/* Open file */
	file = fopen(TMPFILE, "a");
	fprintf(file, "%03d,%02d\n", temp, hume);

	/* Close file */
	fclose(file);
	serial_clean();

	printf("\nsaved.\n");

	return;
}

void
usage()
{
	die(	"usage: sfftv [OPTION]\n"
		"  -b        Baudrate\n"
		"  -d        Device\n"
		"  -v        Show version\n"
		"  -h        Help command\n" );
}

int
main( int argc, char *argv[] )
{
	/* Arguments */
	ARGBEGIN {
	case 'b':
		serialbaud = atoi(EARGF(usage()));
		break;
	case 'd':
		serialdevice = EARGF(usage());
		break;
	case 'v':
		die("datasave-"VERSION"\n");
		return 0;
	default:
		usage();
	} ARGEND

	if (!serialbaud)
		serialbaud = 9600;

	if (!serialdevice)
	{
		printf("No device\n");
		return 1;
	}

	/* Open port */
	if (serial_openport(serialdevice, serialbaud)!=0)
	{
		printf("%s no connect\n", serialdevice);
		return 1;
	}

	listenport();

	return 0;
}
