# datasave

include config.mk

SRC = datasave.c serial.c util.c
OBJ = ${SRC:.c=.o}

all: options datasave

options:
	@echo datasave build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

datasave: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f datasave ${OBJ} datasave-${VERSION}.tar.xz

dist: clean
	mkdir -p datasave-${VERSION}
	cp -R ${SRC} config.h\
		arg.h serial.h util.h\
		Makefile config.mk\
		LICENSE\
		\datasave-${VERSION}
	tar -cf datasave-${VERSION}.tar datasave-${VERSION}
	xz -9 -T0 datasave-${VERSION}.tar
	rm -rf datasave-${VERSION}

install: all
	mkdir -p ${PREFIX}/bin
	cp -f datasave ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/datasave

uninstall:
	rm -r ${PREFIX}/bin/datasave

.PHONY: all options clean dist install uninstall
